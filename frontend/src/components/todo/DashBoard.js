import React from "react";
import Form from "./Form";
import List from "./List";
// --------------------------------

const DashBoard = () => {
  return (
    <>
        <List />
        <Form />
    </>
  );
};

export default DashBoard;
