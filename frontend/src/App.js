import React from "react";
import ReactDOM from "react-dom";
import Header from "./components/layout/Header";
import DashBoard from "./components/todo/DashBoard";
import {Provider} from "react-redux";
import store from "./store";

// -----------------------------------------

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <div className="container">
                    <Header/>
                    <DashBoard/>
                </div>
            </Provider>


        );
    }
}

ReactDOM.render(<App/>, document.getElementById("app"));
